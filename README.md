# test-files

Repository with various legacy Inkscape SVG files to serve as test cases
for (automatic or optional) update routines in Inkscape >= 0.92.

## [line-spacing](line-spacing)

Legacy files with text (regular, flowed) that may be rendered with
different layout in Inkscape >= 0.92. Test cases have been used to
verify external extension and new internal update functions to adjust
line-height, font-size properties as needed to preserve appearance.

See also: Inkscape Wiki - [Line Height Bugs](http://wiki.inkscape.org/wiki/index.php/Line_Height_Bugs)

## [dpi-change](dpi-change)

Legacy files created or last edited with older Inkscape versions which
may require dpi change on load.

## [generic-font-families](generic-font-families)

Inkscape 0.92.x currently renames generic font family names as written
with older Inkscape versions (Sans -> sans-serif, Serif -> serif,
Monospace -> monospace) to allow rendering in modern web browsers using
fonts that better match the author's original intent. The folder
contains test cases to investigate updating the font name in Inkscape's
custom style property '-inkscape-font-specification' as well (may help
to avoid confusing message within Inkscape about font not found).

## [font-size](font-size)

Test cases with failures related to font-size: currently, the built-in
fix unsets font-size for outer text style. The initial value for the
undefined style property might be larger than the font-size used for the
inner styles on the `<tspan/>` elements. A large difference between
outer and inner original font-size values also seems to fail even if the
inner font-size is clearly larger than the inital value for the unset
font-size of the outer style (the fixed line-spacing is too small in
this case).
